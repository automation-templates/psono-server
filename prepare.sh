#!/bin/bash

source ./environment


generate_ssl(){
	if [ "$(ls ssl)" ]; then
        echo "SSL: ssl dir is not empty. Clean it manually if you intend to."
	else
        echo "SSL: Generating SSL Certificates"
        openssl req -new -newkey rsa:4096 -nodes -keyout ssl/$SERVICE_URL.key -out ssl/$SERVICE_URL.csr -subj "/C=US/ST=Denial/L=Springfield/O=Dis/CN=$SERVICE_URL"
        openssl req -new -newkey rsa:4096 -days 365 -nodes -x509 -subj "/C=US/ST=Denial/L=Springfield/O=Dis/CN=$SERVICE_URL" -keyout ssl/$SERVICE_URL.key  -out ssl/$SERVICE_URL.cert
	fi
}

generate_secrets(){
    if [ -f rendered/psono-secrets.txt ]; then
        echo "Secrets file already exists. Not overwriting."
    else 
        echo "Generatig Secrets file."
        $EXECUTOR run --rm -ti psono/psono-server:latest python3 ./psono/manage.py generateserverkeys > rendered/psono-secrets.txt
    fi
}

prepare_server_settings(){
    if [ -f rendered/psono-server-settings.yaml ]; then
        echo "Server config file already exists. Not overwriting."
    else 
        echo "Preparing server config file."
        generate_secrets
        cat rendered/psono-secrets.txt > rendered/psono-server-settings.yaml
        cat base_files/psono-server-settings.yaml >> rendered/psono-server-settings.yaml
        export `cat rendered/psono-secrets.txt | grep DB_SECRET | sed -e 's/: /=/g' -e "s/'//g" | sed -e 's/\r$//'`
        sed -i '' "s/<SERVICE_URL>/$SERVICE_URL/g" rendered/psono-server-settings.yaml
        sed -i '' "s/<DB_SECRET>/$DB_SECRET/g" rendered/psono-server-settings.yaml
    fi
}

prepare_compose_file(){
    echo "Preparing Docker Compose file"
    export `cat rendered/psono-secrets.txt | grep DB_SECRET | sed -e 's/: /=/g' -e "s/'//g" | sed -e 's/\r$//'`
    sed "s/\<DB_SECRET\>/$DB_SECRET/g" base_files/docker-compose.yaml > rendered/docker-compose.yaml
}

prepare_nginx(){
    echo "Preparing Nginx files"
    sed "s/\<SERVICE_URL\>/$SERVICE_URL/g" base_files/psono.conf > rendered/psono.conf
}

prepare_webclient(){
    echo "Preparing Webclient files"
    sed "s/\<SERVICE_URL\>/$SERVICE_URL/g" base_files/webclient-config.json > rendered/webclient-config.json
}

copy_static_files(){
    echo "Copying static files"
    cp base_files/postgres-extensions.sql rendered/postgres-extensions.sql
    cp base_files/privacy-policy-content.html rendered/privacy-policy-content.html
}

generate_ssl
prepare_server_settings
prepare_compose_file
prepare_nginx
prepare_webclient
copy_static_files

echo "Files generated"
echo "Configure your e-mail settings and then"
echo "proceed to rendered directory and run $EXECUTOR-compose up -d"