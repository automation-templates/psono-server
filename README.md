Quickly run a [Psono](https://psono.com/) stack on Docker-Compose or Podman-Compose.

## Instructions
1. Fill your server name (DNS) on [environment](environment). Adjust executor as needed.

2. Run [prepare.sh](prepare.sh).

3. Configure e-mail settings on [rendered/psono-server-settings.yaml](rendered/psono-server-settings.yaml).

4. Go to [rendered](rendered) folder and run `docker-compose up -d` or `podman-compose up -d`

5. Server should be running and accessible through informed server name in a couple minutes.

## SSL
The [prepare.sh](prepare.sh) script will automatically generate a self-signed SSL Certificate-Key pair inside [ssl](ssl) folder ONLY if the folder is empty. Certificate is not replaced on next runs of the script, even if expired.  
You can use your own certificate if you want to. The script expects the certificate and private key to exist inside [ssl](ssl) folder and be named with <SERVICE_URL>.cert and <SERVICE_URL>.key. They also won't be overriden when you run the [prepare.sh](prepare.sh) script.  
  
  
## Useful commands
Following are some commands used to manage the Psono Server. They can be run from inside the `psono-server` container (ex.: using docker exec) or you can use something like `docker run --rm --network psono-network -v ./psono-server-settings.yaml:/root/.psono_server/settings.yaml -ti psono/psono-server:latest` and append the desired command to the end of the line.

- Send test e-mail
```
python3 ./psono/manage.py sendtestmail lucas@maltempimonfardine.com
```
- Include new user
```
python3 ./psono/manage.py createuser username@example.com myPassword email@something.com
```
- Promote user to Admin
```
python3 ./psono/manage.py promoteuser username@example.com superuser
```


## To-do
- Include Psono Fileserver into setup